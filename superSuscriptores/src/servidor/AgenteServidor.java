package servidor;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("serial")
public class AgenteServidor extends Agent {

	/**
	 * Keys: subscribed agents. Values: number of remaining messages
	 */
	private Map<AID, Integer> suscriptores;
	private int freq;

	private class GenerarNumeros extends TickerBehaviour {

		public GenerarNumeros(Agent a, long period) {
			super(a, period);
		}

		@Override
		protected void onTick() {

			ArrayList<AID> eliminar = new ArrayList<>();
			ArrayList<AID> temp = new ArrayList<>(suscriptores.keySet());

			if (suscriptores != null && !suscriptores.isEmpty()) {

				double value = Math.random();

				ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
				msg.setSender(myAgent.getAID());
				msg.setConversationId("randomnumbers");
				msg.setContent(String.valueOf(value));
				for (AID c : temp) {
					msg.addReceiver(c);
				}
				myAgent.send(msg);

				for (AID c : temp) {
					suscriptores.put(c, suscriptores.get(c) - 1);
					if (suscriptores.get(c) == 0) {
						eliminar.add(c);
					}
				}
			}

			for (AID a : eliminar) {
				suscriptores.remove(a);
			}
		}

	}

	private class AtenderSuscripciones extends CyclicBehaviour {

		public AtenderSuscripciones(Agent a) {
			super(a);
		}

		@Override
		public void action() {
			MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.SUBSCRIBE);
			ACLMessage msg = myAgent.receive(mt);
			if (msg != null) {
				int secs = Integer.parseInt(msg.getContent());
				AID ag = msg.getSender();
				suscriptores.put(ag, secs * freq);

				ACLMessage reply = msg.createReply();
				reply.setPerformative(ACLMessage.AGREE);
				reply.setContent("Te has suscrito por " + secs + "segundos");
				myAgent.send(reply);

			} else {
				block();
			}

		}
	}

	@Override
	protected void setup() {
		Object[] args = getArguments();

		if (args != null && args.length == 1) {

			double fr = Float.parseFloat((String) args[0]);
			int tick = (int) Math.ceil(1000 / fr);
			this.freq = (int) fr;

			DFAgentDescription dfd = new DFAgentDescription();
			ServiceDescription sd = new ServiceDescription();
			sd.setType("servidorAleatorio");
			sd.setName(getName());
			sd.setOwnership("ComDis");
			dfd.setName(getAID());
			dfd.addServices(sd);

			suscriptores = new HashMap<>();

			addBehaviour(new GenerarNumeros(this, tick));
			addBehaviour(new AtenderSuscripciones(this));

			try {
				DFService.register(this, dfd);
			} catch (FIPAException e) {
				System.out.println(e.getMessage());
				doDelete();
			}
		} else {
			System.out.println(getLocalName() + "Argumentos err�neos");
			doDelete();
		}

	}

	@Override
	protected void takeDown() {
		try {
			DFService.deregister(this);
		} catch (Exception e) {
		}
		super.takeDown();
	}

}

package cliente.gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.text.DefaultCaret;

@SuppressWarnings("serial")
public class ClienteGui extends JFrame {

	private JPanel contentPane;
	private final JLabel labelTitulo = new JLabel("Cliente");
	private final JLabel labelEstado = new JLabel("Estado");
	private final JTextArea textArea = new JTextArea();
	private final JScrollPane scrollPane = new JScrollPane();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch (Throwable e) {
			e.printStackTrace();
		}
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					ClienteGui frame = new ClienteGui();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ClienteGui() {
		setTitle("Cliente");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		this.labelTitulo.setFont(new Font("Tahoma", Font.BOLD, 11));
		this.labelTitulo.setHorizontalAlignment(SwingConstants.CENTER);

		contentPane.add(this.labelTitulo, BorderLayout.NORTH);
		this.labelEstado.setHorizontalAlignment(SwingConstants.CENTER);
		this.labelEstado.setFont(new Font("Tahoma", Font.ITALIC, 11));
		PropertyChangeListener l = new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent e) {
				setTitle(labelEstado.getText() + " - " + labelTitulo.getText());
			}
		};
		this.labelEstado.addPropertyChangeListener("text", l);

		contentPane.add(this.labelEstado, BorderLayout.SOUTH);

		DefaultCaret caret = (DefaultCaret) textArea.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);

		contentPane.add(this.scrollPane, BorderLayout.CENTER);
		this.scrollPane.setViewportView(this.textArea);
		this.textArea.setEditable(false);
		this.textArea.setText("---------- INICIO DE LOS DATOS ---------");

	}

	public ClienteGui(String nombre) {
		this();
		this.labelTitulo.setText(nombre);
		this.setTitle(nombre);
	}

	public void addDato(String dato) {
		this.textArea.append("\n" + dato.trim());
	}

	public void setEstado(String estado) {
		this.labelEstado.setText(estado.trim());
	}

}

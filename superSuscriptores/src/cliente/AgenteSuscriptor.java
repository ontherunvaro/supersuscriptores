package cliente;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import cliente.gui.ClienteGui;

@SuppressWarnings("serial")
public class AgenteSuscriptor extends Agent {

	private ClienteGui gui;
	private String previousValue;

	private class SuscribirBehaviour extends Behaviour {

		private int seconds;
		private boolean done = false;

		public SuscribirBehaviour(Agent a, int seconds) {
			super(a);
			this.seconds = seconds;
			gui.setEstado("Buscando servidor");
		}

		@Override
		public void action() {
			DFAgentDescription temp = new DFAgentDescription();
			ServiceDescription sd = new ServiceDescription();
			sd.setType("servidorAleatorio");
			temp.addServices(sd);
			try {
				DFAgentDescription[] result = DFService.search(myAgent, temp);
				if (result != null && result.length == 1) {
					AID server = result[0].getName();
					ACLMessage msg = new ACLMessage(ACLMessage.SUBSCRIBE);
					msg.addReceiver(server);
					msg.setContent(String.valueOf(seconds));
					msg.setConversationId("suscription");
					myAgent.send(msg);
					done = true;
					ListenBehaviour lb = new ListenBehaviour(myAgent);
					/*
					 * ListenForEnd se coloca a un segundo porque las
					 * frecuencias vienen en (int) hercios: frecuencia m�nima=1
					 * mensaje/segundo
					 */
					myAgent.addBehaviour(lb);
				}
			} catch (FIPAException e) {
				e.printStackTrace();
			}

		}

		@Override
		public boolean done() {
			return this.done;
		}

	}

	private class ListenBehaviour extends CyclicBehaviour {

		public ListenBehaviour(Agent a) {
			super(a);
			gui.setEstado("Recibiendo datos");
		}

		@Override
		public void onStart() {
			myAgent.addBehaviour(new ListenForEnd(myAgent, 1000));
		};

		@Override
		public void action() {
			MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.INFORM);
			ACLMessage msg = myAgent.receive(mt);
			if (msg != null) {
				gui.addDato(msg.getContent());
				previousValue = msg.getContent();
			} else {
				block();
			}
		}

	}

	/**
	 * Mira previousValue para monitorizar que no cambia Si no cambia durante 2
	 * segundos, se asume que ha finalizado.
	 *
	 * @author �lvaro
	 *
	 */
	private class ListenForEnd extends TickerBehaviour {

		private String storedValue = "";
		private boolean was_equal = false;

		public ListenForEnd(Agent a, long period) {
			super(a, period);
		}

		@Override
		protected void onTick() {
			String temp = new String(previousValue);
			if (temp.equals(storedValue)) {
				if (!was_equal) {
					was_equal = true;
				} else {
					gui.setEstado("Terminado");
					myAgent.doDelete();
				}
			} else {
				storedValue = temp;
				was_equal = false;
			}
		}

	}

	@Override
	protected void setup() {

		Object[] args = getArguments();
		if (args != null && args.length == 1) {
			int seconds = Integer.parseInt((String) args[0]);
			gui = new ClienteGui(getLocalName());
			gui.setVisible(true);
			addBehaviour(new SuscribirBehaviour(this, seconds));
		} else {
			System.out.println(getLocalName() + ": Argumentos err�neos");
			doDelete();
		}

	}

}
